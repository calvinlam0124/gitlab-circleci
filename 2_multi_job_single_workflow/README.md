

# Example

CircleCI will lookup `workflows` first, then breakdown `workflows` to `jobs`.

In this case workflows (my-workflow-build-and-test), breakdown to a jobs (my-job-build) and a jobs (my-job-test)

PS:
the jobs run parallel.
you can use difference docker image or resource class for difference job.

```yaml
version: 2.1

jobs:
  my-job-build:
    docker:
      - image: cimg/base:2023.03
    resource_class: small
    steps:
      - checkout
      - run: echo "this is the build job"
  my-job-test:
    docker:
      - image: docker:17.05.0-ce-git
    resource_class: medium+
    steps:
      - run: echo "this is the test job"
      - checkout
      - run:
          name: Hello world sub-steps
          command: |
            echo "Hello"
            echo "World"
            echo "!"

workflows:
  my-workflow-build-and-test:
    jobs:
      - my-job-build
      - my-job-test
```
