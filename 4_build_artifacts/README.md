# Example
after the circle-ci pipeline compeleted, go to circle-ci page and ARTIFACTS tab, you will see the downlaod link of artifact.

```
version: 2.1

jobs:
  my-job-build:
    docker:
      - image: cimg/base:2023.03
    resource_class: small
    steps:
      - checkout
      - run: echo "this is the build job"
      - run: 
            name: Create Artifact
            command: echo "this-is-artifact-content" > /tmp/artifact_file.txt
      - store_artifacts:
          path: /tmp/artifact_file.txt
          destination: renamed-download-name-artifact-file.txt
  my-job-test:
    docker:
      - image: docker:17.05.0-ce-git
    resource_class: medium+
    steps:
      - run: echo "this is the test job"
      - checkout
      - run:
          name: Hello world sub-steps
          command: |
            echo "Hello"
            echo "World"
            echo "!"

workflows:
  my-workflow-build-and-test:
    jobs:
      - my-job-build
      - my-job-test
```


![1](./images/artifact_1.png)
![2](./images/artifact_2.png)
