# Example
In this case, the job `my-job-build` will trigger only `develop`, `main` and `create-circleci-config` branches. And the job `my-job-test` only trigger by `develop`, `features/*` and `create-circleci-config` branches.

```yaml
version: 2.1

jobs:
  my-job-build:
    docker:
      - image: cimg/base:2023.03
    resource_class: small
    steps:
      - checkout
      - run: echo "this is the build job"
  my-job-test:
    docker:
      - image: docker:17.05.0-ce-git
    resource_class: medium+
    steps:
      - run: echo "this is the test job"
      - checkout
      - run:
          name: Hello world sub-steps
          command: |
            echo "Hello"
            echo "World"
            echo "!"

workflows:
  my-workflow-build-and-test:
    jobs:
      - my-job-build:
          filters:
            branches:
              only:
                - develop
                - main
                - create-circleci-config
      - my-job-test:
          filters:
            branches:
              only:
                - develop
                - /features\/.*/
                - create-circleci-config

```
