
### CircleCI

Update the yaml file to your project_root `.circleci/config.yml`

```yml
version: 2.1

workflows:
  say-hello-workflow:
    jobs:
      - say-hello-job

jobs:
  say-hello-job:
    docker:
      - image: cimg/base:stable
    steps:
      - checkout
      - run:
            name: "Say hello"
            command: |
                echo "Hello"
                echo "World"
                echo "!"
      - run:
            name: "Say hello - 2"
            command: "echo Hello, World 2!"
```
you will get the result like that
![0](./images/basic.png)

