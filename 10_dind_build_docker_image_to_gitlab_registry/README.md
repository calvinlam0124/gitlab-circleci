# Example
Circle
- my-workflow-build (workflow) -> my-job-build (job) -> [branch only: develop / main]
- my-workflow-php-test  (workflow) -> my-job-php-test (job) -> [branch only: develop / feature/*]

MUST be generate Gitlab `Project Deploy Token` for CircleCI push docker image to Gitlab docker registry.
Go to Project > Settings > Repository > Deploy tokens > input fields and click `Create deploy token`
Once you get the token pair, you can verify through below command
```sh
// format
docker login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
// real example
docker login -u gitlab+deploy-token-2244004 -p T_O_K_E_N https://registry.gitlab.com/project/erp
```
![gitlab_deploy_token](./images/0_gitlab_deploy_token.png)
[Gitlab doc](https://docs.gitlab.com/ee/user/project/deploy_tokens/#create-a-deploy-token)
---
MAKE SURE you enabled Gitlab `Container Registry` for your project.
![gitlab_enable_registry](./images/1_gitlab_enable_registry)
[Gitlab doc](https://medal.ctb.upm.es/internal/gitlab/help/user/packages/container_registry/index.md#enable-the-container-registry-for-your-project)

---

```yml
version: 2.1
jobs:
  my-job-build:
    working_directory: /app
    docker:
      - image: docker:17.05.0-ce-git
    steps: 
      - checkout
      - setup_remote_docker
      - run:
          name: CircleCI variables
          command: |
            echo "CIRCLE_BUILD_NUM=$CIRCLE_BUILD_NUM"
            echo "CIRCLE_BRANCH=$CIRCLE_BRANCH"
            echo "CIRCLE_USERNAME=$CIRCLE_USERNAME"
            echo "CIRCLE_WORKFLOW_ID=$CIRCLE_WORKFLOW_ID"
            echo "CIRCLE_WORKFLOW_JOB_ID=$CIRCLE_WORKFLOW_JOB_ID"
            echo "CIRCLE_WORKFLOW_WORKSPACE_ID=$CIRCLE_WORKFLOW_WORKSPACE_ID"
            echo "CIRCLE_WORKING_DIRECTORY=$CIRCLE_WORKING_DIRECTORY"
      - run:
          name: basic info
          command: |
            whoami
            pwd
            ls -alFh
            uname -a
            git status
            date
      - run:
          name: Docker build
          command: |
            docker build -f Dockerfile.devops-deployment -t project/erp:ci-001-$CIRCLE_BUILD_NUM .
      - run:
          name: Docker Tag and Push
          command: |
            docker login -u gitlab+deploy-token-2244004 -p T_O_K_E_N https://registry.gitlab.com/project/erp
            docker tag project/erp:ci-001-$CIRCLE_BUILD_NUM registry.gitlab.com/project/erp:ci-001-$CIRCLE_BUILD_NUM
            docker push registry.gitlab.com/project/erp:ci-001-$CIRCLE_BUILD_NUM
            docker tag project/erp:ci-001-$CIRCLE_BUILD_NUM registry.gitlab.com/project/erp:latest
            docker push registry.gitlab.com/project/erp:latest
  my-job-php-test:
    working_directory: /var/www/html
    docker:
      - image: php:8.2.7-apache-bookworm
        environment:
          APP_ENV: ci-test
          APP_KEY: base64:nNurroXiWXSH9TUF7cSfOQvarWXN+95Mcvl63tDYeVo=
    steps:
      - checkout
      - run:
          name: Install system packages
          command: |
            apt-get update
            apt-get -y install git zip unzip libzip-dev
            docker-php-ext-install zip
      - run:
          name: Display PHP information
          command: |
            php -v
            php -m
            php -i
            php composer.phar --version
      - run:
          name: Composer install
          command: |
            php composer.phar install
      - run:
          name: Laravel pre-script
          command: |
            touch .env
            php artisan env
      - run:
          name: Run Unit tests
          command: ./vendor/bin/phpunit


workflows:
  my-workflow-build:
    jobs:
      - my-job-build:
          filters:
            branches:
              only:
                - develop
                - main
  my-workflow-php-test:
    jobs:
      - my-job-php-test:
          filters:
            branches:
              only:
                - develop
                - /features\/.*/

```
