### Setup
1. given you already had Gitlab account
1. then create a git repo
1. sign up a CircleCI account
1. go to CircleCI main page, it will ask for link your git account, click the button
![0](./0_setup_linkage/images/0_link_git.png)
1. redirect to your git service provider
![1](./0_setup_linkage/images/1_git_authorize.png)
1. it will return to CircleCI console, select your repositry which you would like to integrate with CircleCI and i prefer choose option2 Faster that will help you to create a new branch named `create-circleci-config` and contain CircleCI config file `./.circleci/config.yml`
![2](./0_setup_linkage/images/2_create_link_project.png)
1. thats all! 
1. you can go back to CircleCI console, you will see the pipeline that already ran.
![3](./0_setup_linkage/images/4_dashboard.png)
1. and check the details page
![4](./0_setup_linkage/images/5_pipeline_details_circileci.png)
1. you can also check the status through Gitlab.
![5](./0_setup_linkage/images/5_pipeline_details_gitlab.png)
1. once you commit anything, it always trigger the piple, so you can modify it as your need [./.circleci/config.yml](./.circleci/config.yml)

### more exmaple:
1. [basic pipeline](./1_basic_pipeline)
1. [single workflow contain multiple jobs](./2_multi_job_single_workflow)
1. [trigger pipeline by specific branch](./3_branch_filter_for_workflows)
1. [build artifacts and download manually](./4_build_artifacts)
1. [my real example, use CircleCI dind build docker image and tag/push to gitlab registry](./10_dind_build_docker_image_to_gitlab_registry)


### TODOs
- Gitlab house keeping - gitlab already has own page for set it up
